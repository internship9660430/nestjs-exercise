import { Injectable, NotFoundException } from '@nestjs/common';
import { AuthorDto } from '../common/dtos/author/author.dto';
import { AuthorDatabaseService } from 'src/database/author-database.service';
import { UpdateAuthorDto } from 'src/common/dtos/author/update-author.dto';

@Injectable()
export class AuthorsService {
  constructor(private readonly authorDatabaseService: AuthorDatabaseService) {}

  // GET ALL
  getAuthors() {
    return this.authorDatabaseService.findAllAuthors();
  }

  // GET ONE (ID)
  async getAuthor(authorId: number) {
    const author = await this.authorDatabaseService.findOneAuthor(authorId);

    if (!author) {
      throw new NotFoundException(
        `Author with author id ${authorId} does not exist`,
      );
    }

    return author;
  }

  // GET ONE W/ BOOK
  async getAuthorWithBooks(authorId: number) {
    const author = await this.authorDatabaseService.findOneAuthorWithBooks(
      authorId,
    );

    if (!author) {
      throw new NotFoundException(
        `Author with author id ${authorId} does not exist`,
      );
    }

    return author;
  }

  // ADD
  addAuthor(authorDto: AuthorDto) {
    return this.authorDatabaseService.createAuthor(authorDto);
  }

  // UPDATE ALL
  async updateAllAuthorDetails(authorId: number, authorDto: AuthorDto) {
    await this.getAuthor(authorId);

    const updatedAuthor = this.authorDatabaseService.updateEntireAuthor(
      authorId,
      authorDto,
    );

    return updatedAuthor;
  }

  // UPDATE SOME
  async updateSomeAuthorDetails(
    authorId: number,
    updateAuthorDto: UpdateAuthorDto,
  ) {
    await this.getAuthor(authorId);

    await this.authorDatabaseService.updatePartialAuthor(
      authorId,
      updateAuthorDto,
    );

    return await this.getAuthor(authorId);
  }

  // REMOVE
  async removeAuthor(authorId: number) {
    const author = this.getAuthor(authorId);

    await this.authorDatabaseService.deleteAuthor(authorId);

    return author;
  }
}
