import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Put,
  UseFilters,
  ValidationPipe,
} from '@nestjs/common';
import { AuthorsService } from './authors.service';
import { AuthorDto } from '../common/dtos/author/author.dto';
import { HttpExceptionFilter } from 'src/filters/http-exception.filter';
import { UpdateAuthorDto } from 'src/common/dtos/author/update-author.dto';

@Controller('authors')
@UseFilters(new HttpExceptionFilter())
export class AuthorsController {
  constructor(private readonly authorsService: AuthorsService) {}

  //GET ALL
  @Get()
  getAuthors() {
    return this.authorsService.getAuthors();
  }

  // GET ONE (ID)
  @Get(':authorId')
  getAuthor(@Param('authorId', ParseIntPipe) authorId: number) {
    return this.authorsService.getAuthor(authorId);
  }

  // GET ONE W/ BOOKS
  @Get(':authorId/books')
  getAuthorWithBooks(@Param('authorId', ParseIntPipe) authorId: number) {
    return this.authorsService.getAuthorWithBooks(authorId);
  }

  // ADD
  @Post()
  addAuthor(@Body(new ValidationPipe()) authorDto: AuthorDto) {
    return this.authorsService.addAuthor(authorDto);
  }

  // UPDATE ALL
  @Put(':authorId')
  updateAllAuthorDetails(
    @Param('authorId', ParseIntPipe) authorId: number,
    @Body(new ValidationPipe()) authorDto: AuthorDto,
  ) {
    return this.authorsService.updateAllAuthorDetails(authorId, authorDto);
  }

  // UPDATE SOME
  @Patch(':authorId')
  updateSomeAuthorDetails(
    @Param('authorId', ParseIntPipe) authorId: number,
    @Body(new ValidationPipe()) updateAuthorDto: UpdateAuthorDto,
  ) {
    return this.authorsService.updateSomeAuthorDetails(
      authorId,
      updateAuthorDto,
    );
  }

  // REMOVE
  @Delete(':authorId')
  removeAuthor(@Param('authorId', ParseIntPipe) authorId: number) {
    return this.authorsService.removeAuthor(authorId);
  }
}
