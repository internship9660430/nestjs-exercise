import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BooksModule } from './books/books.module';
import { AuthorsModule } from './authors/authors.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DatabaseModule } from './database/database.module';
import { Author } from './database/entities/Author';
import { Book } from './database/entities/Book';
import { BookAuthor } from './database/entities/BookAuthor';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root', // STORE TO ENV
      password: 'nQE7UDdTuChR+@Sg', // STORE TO ENV
      database: 'books_db',
      entities: [Book, Author, BookAuthor],
      synchronize: true,
    }),
    BooksModule,
    AuthorsModule,
    DatabaseModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
