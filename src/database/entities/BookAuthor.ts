import { Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Author } from './Author';
import { Book } from './Book';

@Entity({ name: 'book_author' })
export class BookAuthor {
  @PrimaryColumn({ name: 'book_id' })
  bookId: number;

  @PrimaryColumn({ name: 'author_id' })
  authorId: number;

  @ManyToOne(() => Book, { onDelete: 'CASCADE' })
  @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
  book: Book[];

  @ManyToOne(() => Author, { onDelete: 'CASCADE' })
  @JoinColumn([{ name: 'author_id', referencedColumnName: 'id' }])
  author: Author[];
}
