import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Author } from './Author';

@Entity({ name: 'books' })
export class Book {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @ManyToMany(() => Author, (author) => author.book, { onDelete: 'CASCADE' })
  @JoinTable({
    name: 'book_author',
    joinColumn: {
      name: 'book_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'author_id',
      referencedColumnName: 'id',
    },
  })
  author?: Author[];

  @Column()
  isbn: string;

  @Column({ nullable: true })
  publisher: string;

  @Column({ nullable: true })
  publicationDate: Date;
}
