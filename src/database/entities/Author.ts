import { Entity, PrimaryGeneratedColumn, Column, ManyToMany } from 'typeorm';
import { Book } from './Book';

@Entity({ name: 'authors' })
export class Author {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  penName: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @ManyToMany(() => Book, (book) => book.author, { onDelete: 'CASCADE' })
  book?: Book[];
}
