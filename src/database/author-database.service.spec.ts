import { Test, TestingModule } from '@nestjs/testing';
import { AuthorDatabaseService } from './author-database.service';

describe('AuthorDatabaseService', () => {
  let service: AuthorDatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AuthorDatabaseService],
    }).compile();

    service = module.get<AuthorDatabaseService>(AuthorDatabaseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
