import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthorDto } from 'src/common/dtos/author/author.dto';
import { Repository } from 'typeorm';
import { Author } from './entities/Author';
import { UpdateAuthorDto } from 'src/common/dtos/author/update-author.dto';

@Injectable()
export class AuthorDatabaseService {
  constructor(
    @InjectRepository(Author) private authorRepository: Repository<Author>,
  ) {}

  findAllAuthors() {
    return this.authorRepository.find();
  }

  findOneAuthor(id: number) {
    return this.authorRepository.findOneBy({ id });
  }

  findOneAuthorWithBooks(id: number) {
    return this.authorRepository.findOne({
      where: { id },
      relations: ['book'],
    });
  }

  async findAuthorsByIdArray(ids: number[]) {
    const authorsTemp = ids.map(async (id) => {
      return await this.findOneAuthor(id);
    });

    return await Promise.all(authorsTemp);
  }

  createAuthor(authorDto: AuthorDto) {
    const newAuthor = this.authorRepository.create(authorDto);
    return this.authorRepository.save(newAuthor);
  }

  updateEntireAuthor(id: number, authorDto: AuthorDto) {
    return this.authorRepository.save({
      ...authorDto,
      id,
    });
  }

  updatePartialAuthor(id: number, updateAuthorDto: UpdateAuthorDto) {
    return this.authorRepository.update({ id }, { ...updateAuthorDto });
  }

  deleteAuthor(id: number) {
    return this.authorRepository.delete({ id });
  }
}
