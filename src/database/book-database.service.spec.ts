import { Test, TestingModule } from '@nestjs/testing';
import { BookDatabaseService } from './book-database.service';

describe('BookDatabaseService', () => {
  let service: BookDatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BookDatabaseService],
    }).compile();

    service = module.get<BookDatabaseService>(BookDatabaseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
