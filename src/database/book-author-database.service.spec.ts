import { Test, TestingModule } from '@nestjs/testing';
import { BookAuthorDatabaseService } from '../book-author-database.service';

describe('BookAuthorDatabaseService', () => {
  let service: BookAuthorDatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BookAuthorDatabaseService],
    }).compile();

    service = module.get<BookAuthorDatabaseService>(BookAuthorDatabaseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
