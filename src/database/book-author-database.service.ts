import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BookAuthor } from './entities/BookAuthor';

@Injectable()
export class BookAuthorDatabaseService {
  constructor(
    @InjectRepository(BookAuthor)
    private bookAuthorRepository: Repository<BookAuthor>,
  ) {}

  async createBookAuthor(bookId: number, authorId: number) {
    const newBookAuthor = this.bookAuthorRepository.create({
      bookId: bookId,
      authorId: authorId,
    });

    return await this.bookAuthorRepository.save(newBookAuthor);
  }

  deleteBookAuthor(bookId: number, authorId: number) {
    return this.bookAuthorRepository.delete({ bookId, authorId });
  }

  deleteAllFromBookId(bookId: number) {
    return this.bookAuthorRepository.delete({ bookId });
  }
}
