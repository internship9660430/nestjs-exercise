import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BookDto } from 'src/common/dtos/book/book.dto';
import { Repository } from 'typeorm';
import { Book } from './entities/Book';
import { AuthorDatabaseService } from './author-database.service';
import { BookAuthorDatabaseService } from './book-author-database.service';
import { BookResponseDto } from 'src/common/dtos/book/book-response.dto';
import { UpdateBookDto } from 'src/common/dtos/book/update-book.dto';

@Injectable()
export class BookDatabaseService {
  constructor(
    @InjectRepository(Book) private bookRepository: Repository<Book>,
    private readonly authorDatabaseService: AuthorDatabaseService,
    private readonly bookAuthorDatabaseService: BookAuthorDatabaseService,
  ) {}

  convertToBookResponseDto(book: Book) {
    const bookDto = new BookResponseDto();
    bookDto.id = book.id;
    bookDto.title = book.title;
    bookDto.isbn = book.isbn;
    bookDto.publisher = book.publisher;
    bookDto.publicationDate = book.publicationDate;

    if (book.author && book.author.length > 0) {
      bookDto.author = book.author.map((author) => author.id);
    } else {
      bookDto.author = [];
    }

    return bookDto;
  }

  async findAllBooks() {
    const books = await this.bookRepository.find({ relations: ['author'] });

    return books.map((b) => this.convertToBookResponseDto(b));
  }

  async findOneBook(id: number) {
    const book = await this.bookRepository.findOne({
      where: { id },
      relations: ['author'],
    });

    return this.convertToBookResponseDto(book);
  }

  findOneBookWithAuthorDetails(id: number) {
    return this.bookRepository.findOne({
      where: { id },
      relations: ['author'],
    });
  }

  async createBook(bookDto: BookDto) {
    const { author, ...otherBookInfo } = bookDto;

    const bookAuthors = await this.authorDatabaseService.findAuthorsByIdArray(
      author,
    );

    const newBook = this.bookRepository.create({ ...otherBookInfo });
    newBook.author = bookAuthors;

    return this.bookRepository.save(newBook);
  }

  async updateEntireBook(id: number, bookDto: BookDto) {
    const { author, ...otherBookInfo } = bookDto;

    const bookAuthors = await this.authorDatabaseService.findAuthorsByIdArray(
      author,
    );

    return this.bookRepository.save({
      author: bookAuthors,
      ...otherBookInfo,
      id,
    });
  }

  async updatePartialBook(id: number, updateBookDto: UpdateBookDto) {
    const { author, ...otherBookInfo } = updateBookDto;

    if (author) {
      await this.bookAuthorDatabaseService.deleteAllFromBookId(id);

      author.map(async (a) => {
        await this.bookAuthorDatabaseService.createBookAuthor(id, a);
      });
    }

    if (Object.keys(otherBookInfo).length !== 0) {
      await this.bookRepository.update({ id }, { ...otherBookInfo });
    }
  }

  deleteBook(id: number) {
    return this.bookRepository.delete({ id });
  }
}
