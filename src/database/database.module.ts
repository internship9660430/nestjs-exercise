import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Author } from './entities/Author';
import { Book } from './entities/Book';
import { BookDatabaseService } from './book-database.service';
import { AuthorDatabaseService } from './author-database.service';
import { BookAuthor } from './entities/BookAuthor';
import { BookAuthorDatabaseService } from './book-author-database.service';

@Module({
  imports: [TypeOrmModule.forFeature([Book, Author, BookAuthor])],
  providers: [
    BookDatabaseService,
    AuthorDatabaseService,
    BookAuthorDatabaseService,
  ],
  exports: [
    BookDatabaseService,
    AuthorDatabaseService,
    BookAuthorDatabaseService,
  ],
})
export class DatabaseModule {}
