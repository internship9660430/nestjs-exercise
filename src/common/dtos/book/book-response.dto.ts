import { IsNumber, IsNotEmpty } from 'class-validator';
import { BookDto } from './book.dto';

export class BookResponseDto extends BookDto {
  @IsNumber()
  @IsNotEmpty()
  id: number;
}
