import { ArrayNotEmpty, IsArray, IsNumber } from 'class-validator';

export class AddRemoveAuthorDto {
  @IsArray()
  @ArrayNotEmpty()
  @IsNumber({}, { each: true })
  authorId: number[];
}
