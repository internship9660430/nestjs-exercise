import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class AuthorDto {
  @IsString()
  @IsNotEmpty()
  penName: string;

  @IsString()
  @IsOptional()
  firstName: string;

  @IsString()
  @IsOptional()
  lastName: string;
}
