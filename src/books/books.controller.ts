import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Put,
  UseFilters,
  ValidationPipe,
} from '@nestjs/common';
import { BooksService } from './books.service';
import { BookDto } from '../common/dtos/book/book.dto';
import { HttpExceptionFilter } from 'src/filters/http-exception.filter';
import { UpdateBookDto } from 'src/common/dtos/book/update-book.dto';
import { AddRemoveAuthorDto } from 'src/common/dtos/book/add-remove-author.dto';

@Controller('books')
@UseFilters(new HttpExceptionFilter())
export class BooksController {
  constructor(private readonly booksService: BooksService) {}

  // GET ALL
  @Get()
  getBooks() {
    return this.booksService.getBooks();
  }

  // GET ONE (ID)
  @Get(':bookId')
  getBook(@Param('bookId', ParseIntPipe) bookId: number) {
    return this.booksService.getBook(bookId);
  }

  // GET ONE W/ AUTHOR DETAILS
  @Get(':bookId/authors')
  getBookWithAuthors(@Param('bookId', ParseIntPipe) bookId: number) {
    return this.booksService.getBookWithAuthors(bookId);
  }

  // ADD
  @Post()
  addBook(@Body(new ValidationPipe({ transform: true })) bookDto: BookDto) {
    return this.booksService.addBook(bookDto);
  }

  // UPDATE ALL
  @Put(':bookId')
  updateAllBookDetails(
    @Param('bookId', ParseIntPipe) bookId: number,
    @Body(new ValidationPipe({ transform: true })) bookDto: BookDto,
  ) {
    return this.booksService.updateAllBookDetails(bookId, bookDto);
  }

  // UPDATE SOME
  @Patch(':bookId')
  updateSomeBookDetails(
    @Param('bookId', ParseIntPipe) bookId: number,
    @Body(new ValidationPipe({ transform: true })) updateBookDto: UpdateBookDto,
  ) {
    return this.booksService.updateSomeBookDetails(bookId, updateBookDto);
  }

  // REMOVE
  @Delete(':bookId')
  removeBook(@Param('bookId', ParseIntPipe) bookId: number) {
    return this.booksService.removeBook(bookId);
  }

  // ADD AUTHOR
  @Post(':bookId/authors')
  addAuthorToBook(
    @Param('bookId', ParseIntPipe) bookId: number,
    @Body(new ValidationPipe()) addAuthorDto: AddRemoveAuthorDto,
  ) {
    return this.booksService.addAuthorToBook(bookId, addAuthorDto);
  }

  // REMOVE AUTHOR
  @Delete(':bookId/authors')
  removeAuthorFromBook(
    @Param('bookId', ParseIntPipe) bookId: number,
    @Body(new ValidationPipe()) removeAuthorDto: AddRemoveAuthorDto,
  ) {
    return this.booksService.removeAuthorFromBook(bookId, removeAuthorDto);
  }
}
