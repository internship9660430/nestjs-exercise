import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { BookDto } from '../common/dtos/book/book.dto';
import { BookDatabaseService } from 'src/database/book-database.service';
import { AuthorDatabaseService } from 'src/database/author-database.service';
import { BookAuthorDatabaseService } from 'src/database/book-author-database.service';
import { AddRemoveAuthorDto } from 'src/common/dtos/book/add-remove-author.dto';
import { UpdateBookDto } from 'src/common/dtos/book/update-book.dto';

@Injectable()
export class BooksService {
  constructor(
    private readonly bookDatabaseService: BookDatabaseService,
    private readonly authorDatabaseService: AuthorDatabaseService,
    private readonly bookAuthorDatabaseService: BookAuthorDatabaseService,
  ) {}

  // GET ALL
  getBooks() {
    return this.bookDatabaseService.findAllBooks();
  }

  // GET ONE (ID)
  async getBook(bookId: number) {
    const book = await this.bookDatabaseService.findOneBook(bookId);

    if (!book) {
      throw new NotFoundException(`Book with book id ${bookId} does not exist`);
    }

    return book;
  }

  // GET ONE W/ AUTHOR DETAILS
  async getBookWithAuthors(bookId: number) {
    const book = await this.bookDatabaseService.findOneBookWithAuthorDetails(
      bookId,
    );

    if (!book) {
      throw new NotFoundException(`Book with book id ${bookId} does not exist`);
    }

    return book;
  }

  // ADD
  async addBook(bookDto: BookDto) {
    await this.testIfAuthorsExist(bookDto.author);
    return this.bookDatabaseService.createBook(bookDto);
  }

  // UPDATE ALL
  async updateAllBookDetails(bookId: number, bookDto: BookDto) {
    await this.getBook(bookId);
    await this.testIfAuthorsExist(bookDto.author);

    const updatedBook = this.bookDatabaseService.updateEntireBook(
      bookId,
      bookDto,
    );

    return updatedBook;
  }

  // UPDATE SOME
  async updateSomeBookDetails(bookId: number, updateBookDto: UpdateBookDto) {
    await this.getBook(bookId);

    if (updateBookDto.author) {
      await this.testIfAuthorsExist(updateBookDto.author);
    }

    await this.bookDatabaseService.updatePartialBook(bookId, updateBookDto);

    return await this.getBookWithAuthors(bookId);
  }

  // REMOVE
  async removeBook(bookId: number) {
    const book = await this.getBook(bookId);

    await this.bookDatabaseService.deleteBook(bookId);

    return book;
  }

  // ADD AUTHOR
  async addAuthorToBook(bookId: number, addAuthorDto: AddRemoveAuthorDto) {
    await this.getBook(bookId);
    await this.testIfAuthorsExist(addAuthorDto.authorId);

    const promises = addAuthorDto.authorId.map(async (a) => {
      await this.bookAuthorDatabaseService.createBookAuthor(bookId, a);
    });

    await Promise.all(promises);

    return await this.getBookWithAuthors(bookId);
  }

  // REMOVE AUTHOR
  async removeAuthorFromBook(
    bookId: number,
    removeAuthorDto: AddRemoveAuthorDto,
  ) {
    await this.getBook(bookId);
    await this.testIfAuthorsExist(removeAuthorDto.authorId);

    const promises = removeAuthorDto.authorId.map(async (a) => {
      await this.bookAuthorDatabaseService.deleteBookAuthor(bookId, a);
    });

    await Promise.all(promises);

    return await this.getBookWithAuthors(bookId);
  }

  async testIfAuthorsExist(authors: number[]) {
    const authorPromises = authors.map(async (a) => {
      const author = await this.authorDatabaseService.findOneAuthor(a);

      if (!author) {
        throw new BadRequestException(
          `Author with author id ${a} does not exist`,
        );
      }
    });

    return await Promise.all(authorPromises);
  }
}
